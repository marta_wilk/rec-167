﻿using ContactBook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactBook.DAL
{
    class ContactBookContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }
    }
}
